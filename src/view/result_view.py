from ..constants import *


class ResultView:
    def __init__(self):
        pass

    def update(self, result_holder):
        self.__print_result(result_holder.results, result_holder.winner)

    def __print_result(self, results, winner):
        if results:
            print()
            for result in results:
                print(MESSAGE_QUARTO + " ---> " + result)
            print(str(winner) + " winner winner chicken dinner!!!")
            print()
