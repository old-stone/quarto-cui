from ..constants import *


class KomaView:
    def __init__(self):
        pass

    def input_koma_no(self, player):
        return input(str(player) + " Choose the koma number. >>>").strip()

    def update(self, koma_holder):
        self.__print_koma_dic(koma_holder.koma_dic)

    def __print_koma_dic(self, koma_dic):
        print()
        [
            print(
                COLOR_HIGHLIGHT + str(key).rjust(2) + RESET
                if koma_dic.get(key).is_chosen else str(key).rjust(2),
                end=" ") for key in koma_dic
        ]
        print()
        [print(" " + str(koma), end=" ") for koma in koma_dic.values()]
        print()
        print()
