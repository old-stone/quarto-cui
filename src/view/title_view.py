import os
from ..constants import *


class TitleView:
    def __init__(self):
        os.system('cls||clear')
        print(MESSAGE_TITLE)

    def update(self, player_holder):
        os.system('cls||clear')
        print(MESSAGE_TITLE)
        for i, player in enumerate(player_holder.players, 1):
            print("Player " + str(i), ":", str(player))

    def input_player_name(self, number):
        return input("Enter player " + str(number) + " name. >>>")
