import os


class TableView:
    def __init__(self):
        pass

    def input_potition(self, player):
        return input(str(player) +
                     ' Enter potition.(ex."0 1") >>>').strip().split()

    def update(self, table_holder):
        self.__print_table_view(table_holder.table)

    def __print_table_view(self, table):
        os.system('cls||clear')
        print()
        table_view = []
        for i in range(0, 9):
            if i == 0:
                table_view.append(" y/x| " +
                                  " | ".join(map(str, range(0, 4))) + " |")
            if i % 2 == 0:
                table_view.append(" ---" + "---".join("+" * 5))
            else:
                table_view.append("  " + str(i // 2) + " | " + " | ".join(
                    [str(koma) if koma else " "
                     for koma in table[i // 2]]) + " |")
        print("\n".join(table_view))
