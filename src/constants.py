from colorama import Fore, Back, Style

# Game Info
BOARD_SIZE = 4

# Color
COLOR_NAME_1 = Style.BRIGHT + Fore.MAGENTA + "MAGENTA" + Style.RESET_ALL
COLOR_FORE_1 = Fore.MAGENTA
COLOR_BACK_1 = Back.MAGENTA

COLOR_NAME_2 = Style.BRIGHT + Fore.GREEN + "GREEN" + Style.RESET_ALL
COLOR_FORE_2 = Fore.GREEN
COLOR_BACK_2 = Back.GREEN

COLOR_HIGHLIGHT = Style.BRIGHT + Back.YELLOW + Fore.BLACK

# Style
BRIGHT = Style.BRIGHT
RESET = Style.RESET_ALL

# Message
MESSAGE_QUARTO = Back.YELLOW + BRIGHT + Fore.YELLOW + "Quarto!!!" + RESET
MESSAGE_OUT_OF_RANGE = Back.YELLOW + Fore.BLACK + "WARN" + RESET + " Out of range."
MESSAGE_ALREADY_PLACED = Back.YELLOW + Fore.BLACK + "WARN" + RESET + " Already placed."
MESSAGE_INVALID_INPUT = Back.YELLOW + Fore.BLACK + "WARN" + RESET + " Invalid input."
MESSAGE_TITLE = '''=================================================
 ,-----.                            ,--.
'  .-.  '  ,--.,--. ,--,--.,--.--.,-'  '-. ,---.
|  | |  |  |  ||  |' ,-.  ||  .--''-.  .-'| .-. |
'  '-'  '-.'  ''  '\ '-'  ||  |     |  |  ' '-' '
 `-----'--' `----'  `--`--'`--'     `--'   `---'
=================================================
'''
