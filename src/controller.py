from .constants import *
from .model.koma_holder import KomaHolder
from .model.player_holder import PlayerHolder
from .model.result_holder import ResultHolder
from .model.table_holder import TableHolder
from .model.vo.koma import Koma
from .view.koma_view import KomaView
from .view.result_view import ResultView
from .view.table_view import TableView
from .view.title_view import TitleView


class Controller:
    def __init__(self):
        self.title_view = TitleView()
        self.table_view = TableView()
        self.koma_view = KomaView()
        self.result_view = ResultView()

    def execute(self):
        # 初期化
        ph, th, kh, rh = self.setup()

        # ゲーム開始
        for i in range(BOARD_SIZE**2):

            # コマを選ぶ
            koma_number, koma = kh.choice(ph.get_dealer())

            # TODO いけてない実装
            # コンソールのため、２要素を更新しないといけないが実装できてないのでゴリ押し
            th.notify_observer()
            kh.notify_observer()

            # ターン交代
            ph.change_turn()

            # コマを置く
            x, y = th.input_potition(ph.get_dealer(), koma)
            th.set_koma(x, y, koma)
            kh.remove(koma_number)
            rh.results = th.judge(x, y)
            print(rh.results)

            if rh.has_ended():
                rh.winner = ph.get_dealer()
                input("press any key...")
                break

    def setup(self):
        # プレイヤー
        ph = PlayerHolder()
        ph.add_observer(self.title_view)
        ph.add_player()
        ph.add_player()

        # テーブル
        th = TableHolder()
        th.add_observer(self.table_view)
        th.table = [[None for j in range(BOARD_SIZE)]
                    for i in range(BOARD_SIZE)]

        # コマ
        kh = KomaHolder()
        kh.add_observer(self.koma_view)
        kh.koma_dic = {
            0: Koma("x", fore=COLOR_FORE_1),
            1: Koma("x", back=COLOR_BACK_1),
            2: Koma("o", fore=COLOR_FORE_1),
            3: Koma("o", back=COLOR_BACK_1),
            4: Koma("X", fore=COLOR_FORE_1),
            5: Koma("X", back=COLOR_BACK_1),
            6: Koma("O", fore=COLOR_FORE_1),
            7: Koma("O", back=COLOR_BACK_1),
            8: Koma("x", fore=COLOR_FORE_2),
            9: Koma("x", back=COLOR_BACK_2),
            10: Koma("o", fore=COLOR_FORE_2),
            11: Koma("o", back=COLOR_BACK_2),
            12: Koma("X", fore=COLOR_FORE_2),
            13: Koma("X", back=COLOR_BACK_2),
            14: Koma("O", fore=COLOR_FORE_2),
            15: Koma("O", back=COLOR_BACK_2)
        }

        rh = ResultHolder()
        rh.add_observer(self.result_view)

        return ph, th, kh, rh
