from ..constants import *
from .abstruct_subject import AbstractSubject
from .vo.koma import Koma


class KomaHolder(AbstractSubject):
    def __init__(self):
        self.__koma_dic = None

    @property
    def koma_dic(self):
        return self.__koma_dic

    @koma_dic.setter
    def koma_dic(self, koma_dic):
        self.__koma_dic = koma_dic
        self.notify_observer()

    def remove(self, koma):
        self.koma_dic.pop(koma)
        self.notify_observer()

    def choice(self, player):
        while (True):
            raw = self.observer.input_koma_no(player)
            if not raw.isdecimal():
                print(MESSAGE_INVALID_INPUT)
                continue
            koma_number = int(raw)
            koma = self.koma_dic.get(koma_number)
            if koma is None:
                print(MESSAGE_INVALID_INPUT)
                continue
            print(koma)
            koma.is_chosen = True
            # TODO いけてない実装につられて無効化
            # self.notify_observer()
            return koma_number, koma
