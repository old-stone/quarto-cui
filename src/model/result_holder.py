from ..constants import *
from .abstruct_subject import AbstractSubject


class ResultHolder(AbstractSubject):
    def __init__(self):
        self.__results = None
        self.__winner = None

    @property
    def results(self):
        return self.__results

    @property
    def winner(self):
        return self.__winner

    @results.setter
    def results(self, results):
        self.__results = results

    @winner.setter
    def winner(self, winner):
        self.__winner = winner
        self.notify_observer()

    def has_ended(self):
        return self.__results