from ..constants import *
from .abstruct_subject import AbstractSubject
from .vo.player import Player


class PlayerHolder(AbstractSubject):
    def __init__(self):
        self.__players = []

    def add_player(self):
        name = self.observer.input_player_name(len(self.__players) + 1)
        if len(self.__players) == 0:
            self.__players.append(Player(name, COLOR_FORE_1, True))
        else:
            self.__players.append(Player(name, COLOR_FORE_2, False))
        self.notify_observer()

    @property
    def players(self):
        return self.__players

    def get_dealer(self):
        return self.__players[0] if self.__players[
            0].is_dealer else self.__players[1]

    def change_turn(self):
        [player.change_turn() for player in self.__players]
