from ..constants import *
from .abstruct_subject import AbstractSubject


class TableHolder(AbstractSubject):
    def __init__(self):
        self.__table = None

    @property
    def table(self):
        return self.__table

    @table.setter
    def table(self, table):
        self.__table = table
        self.notify_observer()

    def input_potition(self, player, koma):
        while True:
            raw = self.observer.input_potition(player)
            if len(raw) is not 2 or not raw[0].isdecimal(
            ) or not raw[1].isdecimal():
                print(MESSAGE_INVALID_INPUT)
                continue
            x, y = map(int, raw)
            if x < 0 or x >= BOARD_SIZE or y < 0 or y >= BOARD_SIZE:
                print(MESSAGE_OUT_OF_RANGE)
                continue
            if self.table[y][x] is not None:
                print(MESSAGE_ALREADY_PLACED)
                continue
            return x, y

    def set_koma(self, x, y, koma):
        self.table[y][x] = koma
        self.notify_observer()

    def judge(self, x, y):
        # 関係するマスを抽出
        yoko, tate, naname = self.__get_relation(x, y)
        # 判定
        results = [
            self.__get_result(koma_list) for koma_list in [tate, yoko, naname]
        ]
        # None除去
        return [e for e in results if e]

    def __get_relation(self, x, y):
        yoko = self.table[x]
        tate = [row[y] for row in self.table]
        naname = []
        if x + y == BOARD_SIZE - 1:
            naname = [
                self.table[i][BOARD_SIZE - 1 - i] for i in range(BOARD_SIZE)
            ]
        elif x == y:
            naname = [self.table[i][i] for i in range(BOARD_SIZE)]
        return yoko, tate, naname

    def __get_result(self, koma_list):
        # 判定をして、処理条件を取得する。
        # 条件にはまらない場合はNone
        if len(koma_list) == 0 or None in koma_list:
            # リストが空（斜めが取得できないとき）orまだ4マスない場合は何もしない
            return
        if False not in [
                koma.fore == COLOR_FORE_1 or koma.back == COLOR_BACK_1
                for koma in koma_list
        ]:
            # 全て1Pカラー
            return COLOR_NAME_1
        if False not in [
                koma.fore == COLOR_FORE_2 or koma.back == COLOR_BACK_2
                for koma in koma_list
        ]:
            # 全て2Pカラー
            return COLOR_NAME_2 + RESET
        if False not in [
                koma.fore == COLOR_FORE_1 or koma.fore == COLOR_FORE_2
                for koma in koma_list
        ]:
            # 全てForeColor
            return "fore color"
        if False not in [
                koma.back == COLOR_BACK_1 or koma.back == COLOR_BACK_2
                for koma in koma_list
        ]:
            # 全てBackColor
            return "back color"
        if [koma.char.upper() for koma in koma_list].count(
                koma_list[0].char.upper()) == BOARD_SIZE:
            # 全て同じ文字
            return koma_list[0].char
        if False not in [koma.char.isupper() for koma in koma_list]:
            # 全てアッパーケース
            return "uppercase"
        if False not in [koma.char.islower() for koma in koma_list]:
            # 全てロワーケース
            return "lowercase"
        return
