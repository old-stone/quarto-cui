class AbstractSubject:
    def __init__(self):
        self.__observer = None

    @property
    def observer(self):
        return self.__observer

    def add_observer(self, observer):
        self.__observer = observer

    def notify_observer(self):
        self.__observer.update(self)
