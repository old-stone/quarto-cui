from colorama import Back, Fore

from ...constants import *


class Koma:
    def __init__(self,
                 char=" ",
                 fore=Fore.BLACK,
                 back=Back.BLACK,
                 is_chosen=False):
        self.fore = fore
        self.back = back
        self.char = char
        self.is_chosen = is_chosen

    def __repr__(self):
        return BRIGHT + self.fore + self.back + self.char + RESET
