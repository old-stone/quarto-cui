from ...constants import *


class Player:
    def __init__(self, name, color, is_dealer=False):
        self.name = name
        self.color = color
        self.is_dealer = is_dealer

    def change_turn(self):
        self.is_dealer = not self.is_dealer

    def __repr__(self):
        return BRIGHT + self.color + "[" + self.name + "]" + RESET
