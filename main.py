import os
import colorama
from src.controller import Controller

if __name__ == '__main__':
    colorama.init(convert=os.name is 'nt', strip=False)
    Controller().execute()
